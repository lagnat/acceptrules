package nl.lolmewn.acceptrules.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import nl.lolmewn.acceptrules.Main;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @author Lolmewn
 */
public class RulesCommand implements CommandExecutor {

    private final Main plugin;
    private final HashMap<UUID, Long> lastCommand = new HashMap<UUID, Long>();

    public RulesCommand(Main plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("You can only use this command as a player!");
            return true;
        }
        Player player = (Player) sender;
        if(!plugin.getAcceptedPlayers().contains(player.getUniqueId().toString()) 
                && lastCommand.containsKey(player.getUniqueId()) 
                && System.currentTimeMillis() - lastCommand.get(player.getUniqueId()) - plugin.getConfig().getDouble("timeBetweenRules", 0) * 1000 < 0){
            player.sendMessage(ChatColor.YELLOW + "Please take your time to read the rules first before going to the next page!");
            return true;
        }
        if(plugin.getConfig().getString("rulesTitle") != null && !plugin.getConfig().getString("rulesTitle").equals("")){
            player.sendMessage(ChatColor.YELLOW + ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("rulesTitle", "====Rules====")));
        }
        if (!plugin.getConfig().getBoolean("usePagination", true)) {
            for (final String page : plugin.getRulesManager().keySet()) {
                for (String rule : plugin.getRulesManager().get(page)) {
                    player.sendMessage(ChatColor.GREEN + ChatColor.translateAlternateColorCodes('&', rule));
                }
                if (plugin.getUsersWhoReadRules().containsKey(player.getName())) {
                    plugin.getUsersWhoReadRules().get(player.getName()).add(page);
                } else {
                    plugin.getUsersWhoReadRules().put(player.getName(), new ArrayList<String>() {
                        {
                            this.add(page);
                        }
                    });
                }
            }
            this.lastCommand.put(player.getUniqueId(), System.currentTimeMillis());
            return true;
        }
        if (args.length == 0 && plugin.getRulesManager().size() != 1) {
            sender.sendMessage(ChatColor.RED + "Incorrect usage, please use /rules <page>");
            String pages = ChatColor.RED + "Available pages: ";
            for (String page : plugin.getRulesManager().keySet()) {
                pages += page + ", ";
            }
            player.sendMessage(pages.substring(0, pages.length() - 2));
            return true;
        }
        try {
            final String page = plugin.getRulesManager().size() != 1 ? args[0] : plugin.getRulesManager().keySet().iterator().next();
            String pageString = page;
            if (!plugin.getRulesManager().containsKey(page)) {
                sender.sendMessage(ChatColor.RED + "No page " + pageString + "!");
                return true;
            }
            if(!plugin.getConfig().getBoolean("higePageCount", false)){
                player.sendMessage(ChatColor.YELLOW + "===Rules page " + pageString + "/" + (plugin.getRulesManager().size()) + "===");
            }
            for (String rule : plugin.getRulesManager().get(page)) {
                player.sendMessage(ChatColor.GREEN + ChatColor.translateAlternateColorCodes('&', rule));
            }
            if (plugin.getUsersWhoReadRules().containsKey(player.getName())) {
                plugin.getUsersWhoReadRules().get(player.getName()).add(page);
            } else {
                plugin.getUsersWhoReadRules().put(player.getName(), new ArrayList<String>() {
                    {
                        this.add(page);
                    }
                });
            }
        } catch (NumberFormatException e) {
            sender.sendMessage(ChatColor.RED + "Page is not a number while it has to be!");
            return true;
        } finally {
            this.lastCommand.put(player.getUniqueId(), System.currentTimeMillis());
        }
        return true;
    }
}
